import { Component, OnInit } from '@angular/core';
import {FaqModel} from "@shared/models/transport/faq/faq.model";

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  list: FaqModel[] = []
  constructor() { }

  ngOnInit(): void {
    this.load();
  }
  private load() {
    this.list = [];

    let news = new FaqModel();
    news.isShow = true;
    news.question = "Для чего предназначена первичная регистрация?";
    news.answer = "Первичная регистрация дает возможность оплатить проезд в общественном транспорте посредством Qr – кода без персонализации транспортной карты.";
    this.list.push(news);

    let news2 = new FaqModel()
    news2.question = "Какова цель полной регистрации?";
    news2.answer = "Есть над чем задуматься: активно развивающиеся страны третьего мира, вне зависимости от их уровня, должны быть представлены в исключительно положительном свете.";
    this.list.push(news2);

    let news3 = new FaqModel()
    news3.question = "Причина загрузки удостоверения личности?";
    news3.answer = "Банальные, но неопровержимые выводы, а также стремящиеся вытеснить традиционное производство, нанотехнологии освещают чрезвычайно интересные особенности картины в целом, однако конкретные выводы, разумеется, призваны к ответу.";
    this.list.push(news3);

    let news4 = new FaqModel()
    news4.question = "Какие возможности дает полная регистрация?";
    news4.answer = "В частности, новая модель организационной деятельности способствует подготовке и реализации системы массового участия.";
    this.list.push(news4);

    let news5 = new FaqModel()
    news5.question = "Доступны ли все функции при привязке дополнительной транспортной карты?";
    news5.answer = "В своём стремлении повысить качество жизни, они забывают, что реализация намеченных плановых заданий однозначно фиксирует необходимость системы обучения кадров, соответствующей насущным потребностям.";
    this.list.push(news5);
  }
}
