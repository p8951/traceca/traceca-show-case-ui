import {Component, OnDestroy, OnInit} from '@angular/core';
import {ICountry, ICountryFilter} from '@shared/models/country';
import {Subject, takeUntil} from 'rxjs';
import {CountryApiService} from '@shared/api/country-api.service';
import {CountryStore} from '@shared/stores/country.store';
import {ILeftMenu} from './left-menu/left-menu';
import {AuthenticationService} from '@coreDirectory/auth/authentication.service';
import {AuthUser} from '@coreDirectory/auth/auth-user';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  menuData: ILeftMenu = {
    title: 'Меню',
    menus: [
      {
        icon: 'fact_check',
        url: 'permits',
        title: 'My permits'
      },
      {
        icon: 'fact_check',
        url: 'map',
        title: 'Map'
      },
      {
        icon: 'fact_check',
        url: 'news',
        title: 'News'
      },
      {
        icon: 'fact_check',
        url: 'faq',
        title: 'FAQ'
      }

    ]
  };
  private unsubscribe = new Subject();
  user: AuthUser;
  userCountry: ICountry | undefined;


  constructor(private countryApi: CountryApiService,
              private countryStore: CountryStore,
              private authService: AuthenticationService) {
    // this.loadCountries();
    this.user = authService.userValue;
  }

  ngOnInit(): void {
  }

  loadCountries(): void {
    const countryFilter: ICountryFilter = {pageNumber: 1, pageSize: 260};
    this.countryApi.getList(countryFilter).pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      if (responseData) {
        this.countryStore.setData(responseData);
        if (this.user.countryId) {
          this.countryStore.getDataById$(this.user.countryId).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
              if (data)
                this.userCountry = data
            });
        }
      }
    })
  }

  logout(): void {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
