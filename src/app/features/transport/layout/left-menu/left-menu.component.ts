import {Component, Input, OnInit} from '@angular/core';
import {ILeftMenu} from "./left-menu";

@Component({
  selector: 'app-left-menu[menuData]',
  templateUrl: './left-menu.component.html',
  styleUrls: ['./left-menu.component.scss']
})
export class LeftMenuComponent implements OnInit {
  @Input() menuData: ILeftMenu = {
    title: 'Default Menu',
    menus: [
      {
        icon: 'user',
        title: 'Default',
        url: '/admin/user-register'
      }
    ]
  };

  constructor() {
  }

  ngOnInit(): void {
  }

}
