import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from "./layout/layout.component";

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'permits',
        loadChildren: () => import('src/app/features/transport/permits/permits.module').then(m => m.PermitsModule)
      },
      {
        path: 'map',
        loadChildren: () => import('src/app/features/transport/map/map.module').then(m => m.MapModule)
      },
      {
        path: 'news',
        loadChildren: () => import('src/app/features/transport/news/news.module').then(m => m.NewsModule)
      },
      {
        path: 'faq',
        loadChildren: () => import('src/app/features/transport/faq/faq.module').then(m => m.FaqModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransportRoutingModule { }
