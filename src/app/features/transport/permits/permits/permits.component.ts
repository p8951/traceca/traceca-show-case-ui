import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {ITransportPermit, ITransportPermitFilter} from "@shared/models/transport-permit";
import {Subject, takeUntil} from "rxjs";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {TransportPermitApiService} from "@shared/api/transport-permit-api.service";
import {PermitsModel} from "@shared/models/transport/permits/permits.model";

@Component({
  selector: 'app-permits',
  templateUrl: './permits.component.html',
  styleUrls: ['./permits.component.scss']
})
export class PermitsComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ITransportPermit> = new MatTableDataSource<ITransportPermit>();
  displayedColumns: string[] = ['permitCode', 'governmentNumber', 'countryB', 'transitCountry', 'date'];
  filter: ITransportPermitFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private transportPermitApi: TransportPermitApiService) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    // this.isLoading = true;

    this.transportPermitApi.getCurrentUserPermitList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.pageSize;
        this.isLoading = false;
      });

    // this.transportPermitApi.getPagedCurrentList(this.filter).pipe(takeUntil(this.unsubscribe))
    //   .subscribe(responseData => {
    //     this.dataSource.data = responseData.items;
    //     this.totalCount = responseData.totalCount;
    //     this.paginator.pageIndex = responseData.currentPage;
    //     this.paginator.length = responseData.totalCount;
    //     this.isLoading = false;
    //   });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }

}
