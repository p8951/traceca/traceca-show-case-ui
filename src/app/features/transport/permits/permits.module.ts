import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermitsRoutingModule } from './permits-routing.module';
import { PermitsComponent } from './permits/permits.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatTableModule} from "@angular/material/table";
import {MatFormFieldModule} from "@angular/material/form-field";
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatInputModule} from "@angular/material/input";
import {MatSortModule} from "@angular/material/sort";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";


@NgModule({
  declarations: [
    PermitsComponent
  ],
  imports: [
    CommonModule,
    PermitsRoutingModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatTableModule,
    MatFormFieldModule,

    MatInputModule,
    MatSortModule,
    MatProgressSpinnerModule
  ]
})
export class PermitsModule { }
