import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PermitsComponent} from "./permits/permits.component";

const routes: Routes = [
  {
    path: '',
    component: PermitsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermitsRoutingModule { }
