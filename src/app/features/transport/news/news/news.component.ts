import { Component, OnInit } from '@angular/core';
import {NewsModel} from "@shared/models/transport/news/news.model";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {
  list: NewsModel[] = [];
  constructor() { }

  ngOnInit(): void {
    this.load();
  }

  private load() {
    this.list = [];
    for (let i = 1; i < 5; i++) {
      let news = new NewsModel()
      news.title = "Qazaq Air возобновляет рейсы в Россию " + i;
      news.description = "Авиакомпания Qazaq Air восстанавливает рейсы в Россию с 13 марта после того, как 12 марта было объявлено об их отмене " + i;
      this.list.push(news);
    }
  }

}
