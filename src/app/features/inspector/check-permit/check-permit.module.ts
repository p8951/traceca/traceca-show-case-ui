import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CheckPermitRoutingModule } from './check-permit-routing.module';
import { CheckPermitComponent } from './check-permit/check-permit.component';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [
    CheckPermitComponent
  ],
  imports: [
    CommonModule,
    CheckPermitRoutingModule,
    MatInputModule,
    FormsModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class CheckPermitModule { }
