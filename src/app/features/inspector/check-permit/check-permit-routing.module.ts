import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CheckPermitComponent} from './check-permit/check-permit.component';

const routes: Routes = [
  {
    path: '',
    component: CheckPermitComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckPermitRoutingModule { }
