import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckPermitComponent } from './check-permit.component';

describe('CheckPermitComponent', () => {
  let component: CheckPermitComponent;
  let fixture: ComponentFixture<CheckPermitComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckPermitComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckPermitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
