import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject, takeUntil} from 'rxjs';
import {TransportPermitApiService} from '@shared/api/transport-permit-api.service';
import {ITransportPermit} from '@shared/models/transport-permit';

@Component({
  selector: 'app-check-permit',
  templateUrl: './check-permit.component.html',
  styleUrls: ['./check-permit.component.scss']
})
export class CheckPermitComponent implements OnInit, OnDestroy {

  permitCode: string = '';
  transportPermit: ITransportPermit | undefined;
  private unsubscribe = new Subject();
  constructor(private api: TransportPermitApiService) { }

  ngOnInit(): void {
  }

  load(permitCode: string): void {
    this.api.getByPermitCode(permitCode).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.transportPermit = responseData;
      })
  }

  search(): void {
    this.load(this.permitCode);
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }

}
