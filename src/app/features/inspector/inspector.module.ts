import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InspectorRoutingModule } from './inspector-routing.module';
import {LayoutComponent} from './layout/layout.component';
import {LeftMenuComponent} from './layout/left-menu/left-menu.component';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {StoresModule} from '@shared/stores/stores.module';


@NgModule({
  declarations: [LayoutComponent, LeftMenuComponent],
  imports: [
    CommonModule,
    InspectorRoutingModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule, StoresModule
  ]
})
export class InspectorModule { }
