import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService} from '@coreDirectory/auth/authentication.service';
import {Subject, takeUntil} from 'rxjs';
import {RoleEnum} from '@shared/enums/role.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  loginForm: FormGroup;
  error: string | null | undefined;
  returnUrl: string = '';
  loading = false;
  private unsubscribe = new Subject();

  constructor(private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private router: Router,
              private authenticationService: AuthenticationService) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f() {
    return this.loginForm.controls;
  }

  submit() {

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.f['username'].value, this.f['password'].value)
      .pipe(takeUntil(this.unsubscribe))
      .subscribe({
        next: () => {
          this.authenticationService.user
            .pipe(takeUntil(this.unsubscribe))
            .subscribe(userData => {
              if (userData) {
                let role = userData.roles?.find(x => x.id === RoleEnum.Admin);
                if (role) {
                  this.router.navigate(['/admin']).then();
                  return;
                }
                role = userData.roles?.find(x => x.id === RoleEnum.Country);
                if (role) {
                  this.router.navigate(['/country']).then();
                  return;
                }
                role = userData.roles?.find(x => x.id === RoleEnum.Carrier);
                if (role) {
                  this.router.navigate(['/carrier']).then();
                  return;
                }
                role = userData.roles?.find(x => x.id === RoleEnum.Transport);
                if (role) {
                  this.router.navigate(['/transport']).then();
                  return;
                }
                role = userData.roles?.find(x => x.id === RoleEnum.Inspector);
                if (role) {
                  this.router.navigate(['/inspector']).then();
                  return;
                }
              }
            })
          // this.router.navigate([this.returnUrl]).then();
        },
        error: error => {
          this.error = error;
          this.loading = false;
        }
      });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }


}
