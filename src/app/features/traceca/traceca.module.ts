import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TracecaRoutingModule } from './traceca-routing.module';
import {LayoutComponent} from './layout/layout.component';
import {LeftMenuComponent} from './layout/left-menu/left-menu.component';
import {MatIconModule} from '@angular/material/icon';
import {StoresModule} from '@shared/stores/stores.module';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';


@NgModule({
  declarations: [LayoutComponent, LeftMenuComponent],
  imports: [
    CommonModule,
    TracecaRoutingModule,
    MatIconModule, StoresModule, MatButtonModule, MatMenuModule
  ]
})
export class TracecaModule { }
