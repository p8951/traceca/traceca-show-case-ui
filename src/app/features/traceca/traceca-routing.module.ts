import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'available-permit',
        loadChildren: () => import('src/app/features/traceca/available-permit/available-permit.module').then(m => m.AvailablePermitModule)
      },
      {
        path: 'permit',
        loadChildren: () => import('src/app/features/traceca/country-couple-permit-count/country-couple-permit-count.module').then(m => m.CountryCouplePermitCountModule)
      },
      {
        path: 'delivery-to-carrier',
        loadChildren: () => import('src/app/features/traceca/permit-carrier/permit-carrier.module').then(m => m.PermitCarrierModule)
      },
      {
        path: 'received-permit',
        loadChildren: () => import('src/app/features/traceca/received-permit/received-permit.module').then(m => m.ReceivedPermitModule)
      },
      {
        path: 'carrier',
        loadChildren: () => import('src/app/features/traceca/country-carrier/country-carrier.module').then(m => m.CountryCarrierModule)
      },
      // {
      //   path: 'delivery-to-carrier',
      //   loadChildren: () => import('src/app/features/traceca/delivery-to-carrier/delivery-to-carrier.module').then(m => m.DeliveryToCarrierModule)
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TracecaRoutingModule { }
