import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {
  ICountryCoupleYearPermitCount,
  ICountryCoupleYearPermitCountFilter
} from '@shared/models/country-couple-year-permit-count';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {CountryCoupleYearPermitCountApiService} from '@shared/api/country-couple-year-permit-count-api.service';
import {AuthenticationService} from '@coreDirectory/auth/authentication.service';

@Component({
  selector: 'app-available-countries',
  templateUrl: './available-countries.component.html',
  styleUrls: ['./available-countries.component.scss']
})
export class AvailableCountriesComponent implements OnInit {

  dataSource: MatTableDataSource<ICountryCoupleYearPermitCount> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'year', 'secondCountry', 'permitCount', 'createdAt'];
  filter: ICountryCoupleYearPermitCountFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  // @ViewChild(MatPaginator)
  // paginator!: MatPaginator;

  constructor(private yearPermitApi: CountryCoupleYearPermitCountApiService,
  private auth: AuthenticationService) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.filter.firstCountryId = this.auth.userValue.countryId;
    this.yearPermitApi.getPagedList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        // this.paginator.pageIndex = responseData.currentPage;
        // this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
