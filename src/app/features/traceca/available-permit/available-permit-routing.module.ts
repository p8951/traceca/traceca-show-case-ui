import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AvailableCountriesComponent} from './available-countries/available-countries.component';

const routes: Routes = [
  {
    path: '',
    component: AvailableCountriesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvailablePermitRoutingModule { }
