import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AvailablePermitRoutingModule } from './available-permit-routing.module';
import { AvailableCountriesComponent } from './available-countries/available-countries.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    AvailableCountriesComponent
  ],
  imports: [
    CommonModule,
    AvailablePermitRoutingModule,
    MatFormFieldModule,
    MatIconModule,
    MatProgressBarModule,
    MatTableModule,
    MatDividerModule,
    MatInputModule
  ]
})
export class AvailablePermitModule { }
