import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CountryCarriersComponent} from './country-carriers/country-carriers.component';

const routes: Routes = [
  {
    path: '',
    component: CountryCarriersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryCarrierRoutingModule { }
