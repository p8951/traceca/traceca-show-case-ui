import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ICountryCouplePermitCount, ICountryCouplePermitCountFilter} from '@shared/models/country-couple-permit-count';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {ICarrier, ICarrierFilter} from '@shared/models/carrier';
import {CarrierApiService} from '@shared/api/carrier-api.service';

@Component({
  selector: 'app-country-carriers',
  templateUrl: './country-carriers.component.html',
  styleUrls: ['./country-carriers.component.scss']
})
export class CountryCarriersComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ICarrier> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'organization', 'contacts'];
  filter: ICarrierFilter = {
    pageNumber: 1,
    pageSize: 100
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private carrierApi: CarrierApiService) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.carrierApi.getPagedCurrentList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }

}
