import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCarriersComponent } from './country-carriers.component';

describe('CountryCarriersComponent', () => {
  let component: CountryCarriersComponent;
  let fixture: ComponentFixture<CountryCarriersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryCarriersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCarriersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
