import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountryCarrierRoutingModule } from './country-carrier-routing.module';
import { CountryCarriersComponent } from './country-carriers/country-carriers.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [
    CountryCarriersComponent
  ],
  imports: [
    CommonModule,
    CountryCarrierRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class CountryCarrierModule { }
