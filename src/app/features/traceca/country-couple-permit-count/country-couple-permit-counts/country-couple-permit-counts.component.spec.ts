import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCouplePermitCountsComponent } from './country-couple-permit-counts.component';

describe('CountryCouplePermitCountsComponent', () => {
  let component: CountryCouplePermitCountsComponent;
  let fixture: ComponentFixture<CountryCouplePermitCountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryCouplePermitCountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCouplePermitCountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
