import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {ICountryCouplePermitCount, ICountryCouplePermitCountFilter} from '@shared/models/country-couple-permit-count';
import {CountryCoupleYearPermitCountApiService} from '@shared/api/country-couple-year-permit-count-api.service';
import {MatDialog} from '@angular/material/dialog';
import {CountryCouplePermitCountApiService} from '@shared/api/country-couple-permit-count-api.service';
import {
  CountryCoupleYearPermitCountCreateComponent
} from '@admin/country-couple-year-permit-count/country-couple-year-permit-count-create/country-couple-year-permit-count-create.component';
import {
  CountryCoupleYearPermitCountAddCountComponent
} from '@admin/country-couple-year-permit-count/country-couple-year-permit-count-add-count/country-couple-year-permit-count-add-count.component';
import {
  CountryCouplePermitCountCreateComponent
} from '../country-couple-permit-count-create/country-couple-permit-count-create.component';

@Component({
  selector: 'app-country-couple-permit-counts',
  templateUrl: './country-couple-permit-counts.component.html',
  styleUrls: ['./country-couple-permit-counts.component.scss']
})
export class CountryCouplePermitCountsComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ICountryCouplePermitCount> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'secondCountry', 'year', 'createdCount', 'availableCount', 'actions'];
  filter: ICountryCouplePermitCountFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private couplePermitApi: CountryCouplePermitCountApiService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.couplePermitApi.getPagedCurrentBList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(CountryCouplePermitCountCreateComponent, {
      width: '500px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.couplePermitApi.create(result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  openDialogAdd(id: number): void {
    const dialogRef = this.dialog.open(CountryCoupleYearPermitCountAddCountComponent, {
      width: '250px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.couplePermitApi.addPermitCount(id, result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }

}
