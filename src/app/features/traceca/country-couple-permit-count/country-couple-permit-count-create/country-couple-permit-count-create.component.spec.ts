import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCouplePermitCountCreateComponent } from './country-couple-permit-count-create.component';

describe('CountryCouplePermitCountCreateComponent', () => {
  let component: CountryCouplePermitCountCreateComponent;
  let fixture: ComponentFixture<CountryCouplePermitCountCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryCouplePermitCountCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCouplePermitCountCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
