import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject, takeUntil} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CountryCouplePermitCountApiService} from '@shared/api/country-couple-permit-count-api.service';
import {
  ICountryCoupleYearPermitCount,
  ICountryCoupleYearPermitCountCreate
} from '@shared/models/country-couple-year-permit-count';
import {CountryCoupleYearPermitCountApiService} from '@shared/api/country-couple-year-permit-count-api.service';
import {ICountryCouplePermitCountCreate} from '@shared/models/country-couple-permit-count';

@Component({
  selector: 'app-country-couple-permit-count-create',
  templateUrl: './country-couple-permit-count-create.component.html',
  styleUrls: ['./country-couple-permit-count-create.component.scss']
})
export class CountryCouplePermitCountCreateComponent implements OnInit, OnDestroy {

  yearPermits: ICountryCoupleYearPermitCount[] = [];
  createForm: FormGroup;
  private unsubscribe = new Subject();
  loading = false;

  constructor(private formBuilder: FormBuilder,
              private yearPermitApi: CountryCoupleYearPermitCountApiService,
              public dialogRef: MatDialogRef<CountryCouplePermitCountCreateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string) {
    this.createForm = this.formBuilder.group({
      countryCoupleYearPermitCountId: ['', Validators.required],
      permitCount: [1, Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadYearPermits();
  }

  loadYearPermits(): void {
    this.yearPermitApi.getCurrentList().pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      if (responseData) {
        this.yearPermits = responseData
      }
    })
  }

  get f() {
    return this.createForm.controls;
  }

  submit(): void {
    if (this.createForm.invalid) {
      return;
    }
    const request: ICountryCouplePermitCountCreate = {
      countryCoupleYearPermitCountId: this.f['countryCoupleYearPermitCountId'].value,
      permitCount: this.f['permitCount'].value
    }
    this.dialogRef.close(request);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  countryChange(): void {
    const countryCoupleYearPermitCountId = this.f['countryCoupleYearPermitCountId'].value;
    const ss = this.yearPermits.find(x => x.id === countryCoupleYearPermitCountId);
    this.f['permitCount'].setValue(ss?.countPermit);
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
