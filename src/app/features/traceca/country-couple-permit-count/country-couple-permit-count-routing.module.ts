import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  CountryCouplePermitCountsComponent
} from './country-couple-permit-counts/country-couple-permit-counts.component';

const routes: Routes = [
  {
    path: '',
    component: CountryCouplePermitCountsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryCouplePermitCountRoutingModule { }
