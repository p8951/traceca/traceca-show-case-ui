import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountryCouplePermitCountRoutingModule } from './country-couple-permit-count-routing.module';
import { CountryCouplePermitCountsComponent } from './country-couple-permit-counts/country-couple-permit-counts.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';
import { CountryCouplePermitCountCreateComponent } from './country-couple-permit-count-create/country-couple-permit-count-create.component';
import {MatDialogModule} from '@angular/material/dialog';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    CountryCouplePermitCountsComponent,
    CountryCouplePermitCountCreateComponent
  ],
  imports: [
    CommonModule,
    CountryCouplePermitCountRoutingModule,
    MatFormFieldModule,
    MatIconModule,
    MatProgressBarModule,
    MatButtonModule,
    MatInputModule,
    MatTableModule,
    MatDividerModule,
    MatPaginatorModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSelectModule
  ]
})
export class CountryCouplePermitCountModule { }
