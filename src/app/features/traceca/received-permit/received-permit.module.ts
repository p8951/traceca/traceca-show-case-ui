import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReceivedPermitRoutingModule } from './received-permit-routing.module';
import { ReceivedPermitsComponent } from './received-permits/received-permits.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    ReceivedPermitsComponent
  ],
  imports: [
    CommonModule,
    ReceivedPermitRoutingModule,
    MatFormFieldModule,
    MatProgressBarModule,
    MatTableModule,
    MatIconModule,
    MatDividerModule,
    MatPaginatorModule,
    MatInputModule
  ]
})
export class ReceivedPermitModule { }
