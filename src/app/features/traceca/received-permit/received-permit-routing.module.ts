import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReceivedPermitsComponent} from './received-permits/received-permits.component';

const routes: Routes = [
  {
    path: '',
    component: ReceivedPermitsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceivedPermitRoutingModule { }
