import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceivedPermitsComponent } from './received-permits.component';

describe('ReceivedPermitsComponent', () => {
  let component: ReceivedPermitsComponent;
  let fixture: ComponentFixture<ReceivedPermitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReceivedPermitsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceivedPermitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
