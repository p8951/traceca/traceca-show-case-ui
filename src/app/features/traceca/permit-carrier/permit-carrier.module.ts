import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermitCarrierRoutingModule } from './permit-carrier-routing.module';
import { PermitCarriersComponent } from './permit-carriers/permit-carriers.component';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTableModule} from '@angular/material/table';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import { PermitCarrierCreateComponent } from './permit-carrier-create/permit-carrier-create.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';



@NgModule({
  declarations: [
    PermitCarriersComponent,
    PermitCarrierCreateComponent
  ],
  imports: [
    CommonModule,
    PermitCarrierRoutingModule,
    MatIconModule,
    MatDividerModule,
    MatPaginatorModule,
    MatTableModule,
    MatProgressBarModule,
    MatFormFieldModule, MatDialogModule, MatInputModule, MatButtonModule, ReactiveFormsModule, MatSelectModule
  ]
})
export class PermitCarrierModule { }
