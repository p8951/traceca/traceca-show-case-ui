import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {
  CountryCouplePermitCountCreateComponent
} from '../../country-couple-permit-count/country-couple-permit-count-create/country-couple-permit-count-create.component';
import {
  CountryCoupleYearPermitCountAddCountComponent
} from '@admin/country-couple-year-permit-count/country-couple-year-permit-count-add-count/country-couple-year-permit-count-add-count.component';
import {AuthenticationService} from '@coreDirectory/auth/authentication.service';
import {ICarrierPermitCount, ICarrierPermitCountFilter} from '@shared/models/carrier-permit-count';
import {CarrierPermitCountApiService} from '@shared/api/carrier-permit-count-api.service';
import {PermitCarrierCreateComponent} from '../permit-carrier-create/permit-carrier-create.component';

@Component({
  selector: 'app-permit-carriers',
  templateUrl: './permit-carriers.component.html',
  styleUrls: ['./permit-carriers.component.scss']
})
export class PermitCarriersComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ICarrierPermitCount> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'carrier', 'country', 'permitCount', 'actions'];
  filter: ICarrierPermitCountFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private carrierPermitCountApi: CarrierPermitCountApiService,
              public dialog: MatDialog,
              private authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.carrierPermitCountApi.getPagedCurrentList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(PermitCarrierCreateComponent, {
      width: '500px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.carrierPermitCountApi.create(result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  openDialogAdd(id: number): void {
    const dialogRef = this.dialog.open(CountryCoupleYearPermitCountAddCountComponent, {
      width: '250px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.carrierPermitCountApi.addPermitCount(id, result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }


  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
