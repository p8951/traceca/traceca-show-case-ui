import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermitCarriersComponent } from './permit-carriers.component';

describe('PermitCarriersComponent', () => {
  let component: PermitCarriersComponent;
  let fixture: ComponentFixture<PermitCarriersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermitCarriersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PermitCarriersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
