import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {ICountryCoupleYearPermitCount} from '@shared/models/country-couple-year-permit-count';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject, takeUntil} from 'rxjs';
import {ICountryCouplePermitCount, ICountryCouplePermitCountCreate} from '@shared/models/country-couple-permit-count';
import {CountryCoupleYearPermitCountApiService} from '@shared/api/country-couple-year-permit-count-api.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {CountryCouplePermitCountApiService} from '@shared/api/country-couple-permit-count-api.service';
import {ICarrierPermitCountCreate} from '@shared/models/carrier-permit-count';
import {CarrierApiService} from '@shared/api/carrier-api.service';
import {ICarrier, ICarrierFilter} from '@shared/models/carrier';

@Component({
  selector: 'app-permit-carrier-create',
  templateUrl: './permit-carrier-create.component.html',
  styleUrls: ['./permit-carrier-create.component.scss']
})
export class PermitCarrierCreateComponent implements OnInit, OnDestroy {

  countryCouplePermits: ICountryCouplePermitCount[] = [];
  createForm: FormGroup;
  carriers: ICarrier[] = [];
  private unsubscribe = new Subject();
  loading = false;

  constructor(private formBuilder: FormBuilder,
              private permitApi: CountryCouplePermitCountApiService,
              public dialogRef: MatDialogRef<PermitCarrierCreateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private carrierApi: CarrierApiService) {
    this.createForm = this.formBuilder.group({
      countryCouplePermitCountId: ['', Validators.required],
      permitCount: [1, Validators.required],
      carrierId: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadPermits();
    this.loadCarriers();
  }

  loadCarriers(): void {
    const request: ICarrierFilter = {
      pageNumber: 1,
      pageSize: 100
    };
    this.carrierApi.getPagedCurrentList(request).pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      if (responseData) {
        this.carriers = responseData.items
      }
    })
  }
  loadPermits(): void {

    this.permitApi.getCurrent().pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      if (responseData) {
        this.countryCouplePermits = responseData
      }
    })
  }

  get f() {
    return this.createForm.controls;
  }

  submit(): void {
    if (this.createForm.invalid) {
      return;
    }
    const request: ICarrierPermitCountCreate = {
      countryCouplePermitCountId: this.f['countryCouplePermitCountId'].value,
      permitCount: this.f['permitCount'].value,
      carrierId: this.f['carrierId'].value
    }
    this.dialogRef.close(request);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  countryChange(): void {
    const countryCouplePermitCountId = this.f['countryCouplePermitCountId'].value;
    const ss = this.countryCouplePermits.find(x => x.id === countryCouplePermitCountId);
    this.f['permitCount'].setValue(ss?.permitCount);
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
