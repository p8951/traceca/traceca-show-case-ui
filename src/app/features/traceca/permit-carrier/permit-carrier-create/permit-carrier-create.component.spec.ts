import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermitCarrierCreateComponent } from './permit-carrier-create.component';

describe('PermitCarrierCreateComponent', () => {
  let component: PermitCarrierCreateComponent;
  let fixture: ComponentFixture<PermitCarrierCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermitCarrierCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PermitCarrierCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
