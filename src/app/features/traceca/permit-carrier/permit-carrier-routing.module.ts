import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PermitCarriersComponent} from './permit-carriers/permit-carriers.component';

const routes: Routes = [
  {
    path: '',
    component: PermitCarriersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermitCarrierRoutingModule { }
