import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryToCarrierRoutingModule } from './delivery-to-carrier-routing.module';
import { DeliveryToCarriersComponent } from './delivery-to-carriers/delivery-to-carriers.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [
    DeliveryToCarriersComponent
  ],
  imports: [
    CommonModule,
    DeliveryToCarrierRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class DeliveryToCarrierModule { }
