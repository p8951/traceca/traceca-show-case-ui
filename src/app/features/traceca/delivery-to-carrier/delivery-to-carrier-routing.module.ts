import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DeliveryToCarriersComponent} from './delivery-to-carriers/delivery-to-carriers.component';

const routes: Routes = [
  {
    path: '',
    component: DeliveryToCarriersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryToCarrierRoutingModule { }
