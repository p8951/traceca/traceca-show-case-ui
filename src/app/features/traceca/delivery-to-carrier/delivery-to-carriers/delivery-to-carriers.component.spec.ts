import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryToCarriersComponent } from './delivery-to-carriers.component';

describe('DeliveryToCarriersComponent', () => {
  let component: DeliveryToCarriersComponent;
  let fixture: ComponentFixture<DeliveryToCarriersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryToCarriersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryToCarriersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
