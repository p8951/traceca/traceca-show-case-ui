import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ICarrier, ICarrierFilter} from '@shared/models/carrier';
import {Subject} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-delivery-to-carriers',
  templateUrl: './delivery-to-carriers.component.html',
  styleUrls: ['./delivery-to-carriers.component.scss']
})
export class DeliveryToCarriersComponent implements OnInit {

  dataSource: MatTableDataSource<ICarrier> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'organization', 'contacts'];
  filter: ICarrierFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor() { }

  ngOnInit(): void {
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  load(): void {}


  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

}
