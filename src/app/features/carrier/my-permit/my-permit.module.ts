import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyPermitRoutingModule } from './my-permit-routing.module';
import { MyPermitsComponent } from './my-permits/my-permits.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import { MyPermitCreateComponent } from './my-permit-create/my-permit-create.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    MyPermitsComponent,
    MyPermitCreateComponent
  ],
  imports: [
    CommonModule,
    MyPermitRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatProgressBarModule,
    MatTableModule,
    MatDividerModule,
    MatPaginatorModule, MatDialogModule, ReactiveFormsModule, MatSelectModule
  ]
})
export class MyPermitModule { }
