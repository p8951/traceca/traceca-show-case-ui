import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ICarrierPermitCount, ICarrierPermitCountFilter} from '@shared/models/carrier-permit-count';
import {Subject, takeUntil} from 'rxjs';
import {ITransportPermit, ITransportPermitFilter} from '@shared/models/transport-permit';
import {TransportPermitApiService} from '@shared/api/transport-permit-api.service';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {
  PermitCarrierCreateComponent
} from '../../../traceca/permit-carrier/permit-carrier-create/permit-carrier-create.component';
import {MyPermitCreateComponent} from '../my-permit-create/my-permit-create.component';

@Component({
  selector: 'app-my-permits',
  templateUrl: './my-permits.component.html',
  styleUrls: ['./my-permits.component.scss']
})
export class MyPermitsComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ITransportPermit> = new MatTableDataSource();
  displayedColumns: string[] = ['permitCode', 'driver', 'model', 'governmentNumber', 'firstCountry', 'used'];
  filter: ITransportPermitFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private transportPermitApi: TransportPermitApiService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.transportPermitApi.getPagedCurrentList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MyPermitCreateComponent, {
      width: '500px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.transportPermitApi.create(result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
