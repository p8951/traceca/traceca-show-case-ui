import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject, takeUntil} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ICarrierPermitCount, ICarrierPermitCountCreate} from '@shared/models/carrier-permit-count';
import {CarrierPermitCountApiService} from '@shared/api/carrier-permit-count-api.service';
import {IPagedRequest} from '@shared/models/base/paged-list';
import {TransportApiService} from '@shared/api/transport-api.service';
import {ITransport, ITransportFilter} from '@shared/models/transport';
import {ITransportPermitCreate} from '@shared/models/transport-permit';

@Component({
  selector: 'app-my-permit-create',
  templateUrl: './my-permit-create.component.html',
  styleUrls: ['./my-permit-create.component.scss']
})
export class MyPermitCreateComponent implements OnInit, OnDestroy {

  createForm: FormGroup;
  private unsubscribe = new Subject();
  loading = false;

  transports: ITransport[] =[];

  permitCarriers: ICarrierPermitCount[] = [];

  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<MyPermitCreateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private carrierPermitCountApi: CarrierPermitCountApiService,
              private transportApi: TransportApiService) {
    this.createForm = this.formBuilder.group({
      countryCoupleYearPermitCountId: ['', Validators.required],
      transportId: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadPermitCarriers();
    this.loadTransport();
  }

  loadPermitCarriers(): void {
    const filter: IPagedRequest = {
      pageNumber: 1,
      pageSize: 100
    };
    this.carrierPermitCountApi.getPagedCurrentList(filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        if (responseData) {
          this.permitCarriers = responseData.items
        }
      })
  }

  loadTransport(): void {
    const filter: ITransportFilter = {
      pageNumber: 1,
      pageSize: 100
    }
    this.transportApi.getPagedCurrentList(filter).pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      if (responseData) {
        this.transports = responseData.items;
      }
    })
  }

  get f() {
    return this.createForm.controls;
  }

  submit(): void {
    if (this.createForm.invalid) {
      return;
    }
    const request: ITransportPermitCreate = {
      countryCoupleYearPermitCountId: this.f['countryCoupleYearPermitCountId'].value,
      transportId: this.f['transportId'].value
    }
    this.dialogRef.close(request);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }

}
