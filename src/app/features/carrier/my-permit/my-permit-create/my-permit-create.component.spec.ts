import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyPermitCreateComponent } from './my-permit-create.component';

describe('MyPermitCreateComponent', () => {
  let component: MyPermitCreateComponent;
  let fixture: ComponentFixture<MyPermitCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyPermitCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyPermitCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
