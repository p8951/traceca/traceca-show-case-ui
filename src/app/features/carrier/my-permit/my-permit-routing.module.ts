import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {MyPermitsComponent} from './my-permits/my-permits.component';

const routes: Routes = [
  {
    path: '',
    component: MyPermitsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyPermitRoutingModule { }
