import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from './layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'available-permit',
        loadChildren: () => import('src/app/features/carrier/available-permit/available-permit.module').then(m => m.AvailablePermitModule)
      },
      {
        path: 'transport',
        loadChildren: () => import('src/app/features/carrier/transport/transport.module').then(m => m.TransportModule)
      },
      {
        path: 'driver',
        loadChildren: () => import('src/app/features/carrier/driver/driver.module').then(m => m.DriverModule)
      },
      {
        path: 'my-permit',
        loadChildren: () => import('src/app/features/carrier/my-permit/my-permit.module').then(m => m.MyPermitModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarrierRoutingModule { }
