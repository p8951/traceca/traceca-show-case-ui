import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject, takeUntil} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ICarrierPermitCountCreate} from '@shared/models/carrier-permit-count';
import {ITransportCreate} from '@shared/models/transport';
import {DriverApiService} from '@shared/api/driver-api.service';
import {IDriver, IDriverFilter} from '@shared/models/driver';

@Component({
  selector: 'app-transport-create',
  templateUrl: './transport-create.component.html',
  styleUrls: ['./transport-create.component.scss']
})
export class TransportCreateComponent implements OnInit {

  createForm: FormGroup;
  private unsubscribe = new Subject();
  loading = false;
  drivers: IDriver[] | undefined;

  constructor(private formBuilder: FormBuilder,
              public dialogRef: MatDialogRef<TransportCreateComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private driverApi: DriverApiService) {
    this.createForm = this.formBuilder.group({
      governmentNumber: ['', Validators.required],
      userId: ['', Validators.required],
      model: ['', Validators.required]
    });
  }


  ngOnInit(): void {
    this.loadDrivers();
  }

  loadDrivers(): void {
    const request: IDriverFilter = {
      pageNumber: 1,
      pageSize: 100
    };

    this.driverApi.getList(request).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.drivers = responseData
      });
  }

  get f() {
    return this.createForm.controls;
  }

  submit(): void {
    if (this.createForm.invalid) {
      return;
    }
    const driver = this.drivers?.find( value => value.userId == this.f['userId'].value);
    const request: ITransportCreate = {
      governmentNumber: this.f['governmentNumber'].value,
      userId: this.f['userId'].value,
      model: this.f['model'].value,
      driver: driver?.lastName + ' ' + driver?.firstName
    }
    this.dialogRef.close(request);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
