import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TransportRoutingModule} from './transport-routing.module';
import {TransportsComponent} from './transports/transports.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatDialogModule} from '@angular/material/dialog';
import {TransportCreateComponent} from './transport-create/transport-create.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';


@NgModule({
  declarations: [
    TransportsComponent,
    TransportCreateComponent
  ],
    imports: [
        CommonModule,
        TransportRoutingModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatIconModule,
        MatProgressBarModule,
        MatTableModule,
        MatDividerModule,
        MatPaginatorModule, MatDialogModule, ReactiveFormsModule, MatSelectModule
    ]
})
export class TransportModule {
}
