import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ICarrierPermitCount, ICarrierPermitCountFilter} from '@shared/models/carrier-permit-count';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {ITransport, ITransportFilter} from '@shared/models/transport';
import {TransportApiService} from '@shared/api/transport-api.service';
import {
  PermitCarrierCreateComponent
} from '../../../traceca/permit-carrier/permit-carrier-create/permit-carrier-create.component';
import {MatDialog} from '@angular/material/dialog';
import {TransportCreateComponent} from '../transport-create/transport-create.component';

@Component({
  selector: 'app-transports',
  templateUrl: './transports.component.html',
  styleUrls: ['./transports.component.scss']
})
export class TransportsComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ITransport> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'driver', 'model', 'governmentNumber'];
  filter: ITransportFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private transportApi: TransportApiService, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.transportApi.getPagedCurrentList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TransportCreateComponent, {
      width: '500px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.transportApi.create(result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
