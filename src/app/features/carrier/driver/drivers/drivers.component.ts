import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ITransport, ITransportFilter} from '@shared/models/transport';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {TransportCreateComponent} from '../../transport/transport-create/transport-create.component';
import {TransportApiService} from '@shared/api/transport-api.service';
import {MatDialog} from '@angular/material/dialog';
import {DriverApiService} from '@shared/api/driver-api.service';
import {DriverCreateComponent} from '../driver-create/driver-create.component';
import {IDriver} from '@shared/models/driver';

@Component({
  selector: 'app-drivers',
  templateUrl: './drivers.component.html',
  styleUrls: ['./drivers.component.scss']
})
export class DriversComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<IDriver> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'fullName', 'username', 'contacts', 'email', 'isActive'];
  filter: ITransportFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private driverApi: DriverApiService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.driverApi.getPagedList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    // this.load();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DriverCreateComponent, {
      width: '500px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.driverApi.create(result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }

}
