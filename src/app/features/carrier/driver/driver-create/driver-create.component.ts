import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {IUserRegisterCreate} from '@shared/models/user-register';
import {IDriverCreate} from '@shared/models/driver';
import {DriverApiService} from '@shared/api/driver-api.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-driver-create',
  templateUrl: './driver-create.component.html',
  styleUrls: ['./driver-create.component.scss']
})
export class DriverCreateComponent implements OnInit {

  registerForm: FormGroup;
  private unsubscribe = new Subject();
  hide = true;
  loading = false;

  constructor(private formBuilder: FormBuilder, private driverApi: DriverApiService,
              public dialogRef: MatDialogRef<DriverCreateComponent>) {
    this.registerForm = this.formBuilder.group({
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      userName: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required],
      contacts: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  get f() {
    return this.registerForm.controls;
  }

  submit(): void {
    if (this.registerForm.invalid) {
      return;
    }
    const request: IDriverCreate = {
      lastName: this.f['lastName'].value,
      firstName: this.f['firstName'].value,
      username: this.f['userName'].value,
      password: this.f['password'].value,
      email: this.f['email'].value,
      contacts: this.f['contacts'].value,
    }
    this.loading = true;
    this.dialogRef.close(request);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }


}
