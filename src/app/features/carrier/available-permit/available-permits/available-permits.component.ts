import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {ICarrierPermitCount, ICarrierPermitCountFilter} from '@shared/models/carrier-permit-count';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {CarrierPermitCountApiService} from '@shared/api/carrier-permit-count-api.service';

@Component({
  selector: 'app-available-permits',
  templateUrl: './available-permits.component.html',
  styleUrls: ['./available-permits.component.scss']
})
export class AvailablePermitsComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ICarrierPermitCount> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'carrier', 'country', 'permitCount'];
  filter: ICarrierPermitCountFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private carrierPermitCountApi: CarrierPermitCountApiService) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.carrierPermitCountApi.getPagedCurrentList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }
  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }

}
