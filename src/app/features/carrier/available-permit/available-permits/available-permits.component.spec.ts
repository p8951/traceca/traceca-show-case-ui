import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AvailablePermitsComponent } from './available-permits.component';

describe('AvailablePermitsComponent', () => {
  let component: AvailablePermitsComponent;
  let fixture: ComponentFixture<AvailablePermitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AvailablePermitsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AvailablePermitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
