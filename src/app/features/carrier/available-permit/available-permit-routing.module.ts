import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AvailablePermitsComponent} from './available-permits/available-permits.component';

const routes: Routes = [
  {
    path: '',
    component: AvailablePermitsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvailablePermitRoutingModule { }
