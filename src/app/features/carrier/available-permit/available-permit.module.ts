import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AvailablePermitRoutingModule } from './available-permit-routing.module';
import { AvailablePermitsComponent } from './available-permits/available-permits.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [
    AvailablePermitsComponent
  ],
  imports: [
    CommonModule,
    AvailablePermitRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatProgressBarModule,
    MatTableModule,
    MatPaginatorModule
  ]
})
export class AvailablePermitModule { }
