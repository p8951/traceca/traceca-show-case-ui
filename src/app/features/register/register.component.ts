import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '@coreDirectory/auth/authentication.service';
import {ICountry, ICountryFilter} from '@shared/models/country';
import {CountryApiService} from '@shared/api/country-api.service';
import {Subject, takeUntil} from 'rxjs';
import {RoleEnum} from '@shared/enums/role.enum';
import {Router} from '@angular/router';
import {IUserRegister, IUserRegisterCreate} from '@shared/models/user-register';
import {UserRegisterApiService} from '@shared/api/user-register-api.service';
import {MatRadioChange} from '@angular/material/radio';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  registerForm: FormGroup;
  countries: ICountry[] | undefined;
  carrierFieldsShow = false;
  private unsubscribe = new Subject();
  role = {
    carrier: RoleEnum.Carrier,
    country: RoleEnum.Country
  }
  floatLabelControl = new FormControl(this.role.country, Validators.required);
  hide = true;
  loading = false;

  constructor(private formBuilder: FormBuilder, private authenticationService: AuthenticationService,
              private countryApi: CountryApiService,
              private router: Router,
              private userRegisterApi: UserRegisterApiService) {
    this.registerForm = this.formBuilder.group({
      countryId: ['', Validators.required],
      lastName: ['', Validators.required],
      firstName: ['', Validators.required],
      patronymic: ['', Validators.required],
      userName: ['', Validators.required],
      password: ['', Validators.required],
      email: ['', Validators.required],
      role: this.floatLabelControl,
      organization: ['', Validators.required],
      contacts: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadCountries();
  }

  loadCountries(): void {
    const countryFilter: ICountryFilter = {pageNumber: 1, pageSize: 260};
    this.countryApi.getPagedList(countryFilter).pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      this.countries = responseData.items
    })
  }

  submit(): void {
    if (this.registerForm.invalid) {
      return;
    }
    const request: IUserRegisterCreate = {
      lastName: this.f['lastName'].value,
      firstName: this.f['firstName'].value,
      patronymic: this.f['patronymic'].value,
      userName: this.f['userName'].value,
      password: this.f['password'].value,
      email: this.f['email'].value,
      role: this.f['role'].value,
      countryId: this.f['countryId'].value,
      contacts: this.f['contacts'].value,
    }
    if (this.carrierFieldsShow) {
      request.address = this.f['address'].value;
      request.city = this.f['city'].value;
      request.organization = this.f['organization'].value;
    }
    this.loading = true;
    this.userRegisterApi.create(request).pipe(takeUntil(this.unsubscribe)).subscribe(data => {
      this.loading = false;
      this.router.navigate(['/login']).then();
    });

  }

  get f() {
    return this.registerForm.controls;
  }

  changeRole(changeValue: MatRadioChange): void {
    if (changeValue.value == RoleEnum.Country) {
      this.carrierFieldsShow = false;
      this.removeCarrierFields();
    } else
    if (changeValue.value == RoleEnum.Carrier) {
      this.addCarrierFields();
      this.carrierFieldsShow = true;
    }
  }

  removeCarrierFields(): void {
    this.registerForm.removeControl('organization');
    this.registerForm.removeControl('address');
    this.registerForm.removeControl('city');
  }

  addCarrierFields(): void {
    this.registerForm.addControl('organization', this.formBuilder.control('', [Validators.required]));
    this.registerForm.addControl('address', this.formBuilder.control('', [Validators.required]));
    this.registerForm.addControl('city', this.formBuilder.control('', [Validators.required]));
  }
  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
