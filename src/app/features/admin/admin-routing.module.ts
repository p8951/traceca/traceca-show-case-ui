import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LayoutComponent} from '@admin/layout/layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: 'user-register',
        loadChildren: () => import('src/app/features/admin/user-register/user-register.module').then(m => m.UserRegisterModule)
      },
      {
        path: 'couple-permit',
        loadChildren: () => import('src/app/features/admin/country-couple-year-permit-count/country-couple-year-permit-count.module').then(m => m.CountryCoupleYearPermitCountModule)
      },
      {
        path: 'permit',
        loadChildren: () => import('src/app/features/admin/permit-history/permit-history.module').then(m => m.PermitHistoryModule)
      },
      {
        path: 'country',
        loadChildren: () => import('src/app/features/admin/country/country.module').then(m => m.CountryModule)
      },
      {
        path: 'user',
        loadChildren: () => import('src/app/features/admin/user/user.module').then(m => m.UserModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
