import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PermitHistoryRoutingModule } from './permit-history-routing.module';
import { PermitHistoriesComponent } from './permit-histories/permit-histories.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';
import { PermitHistoryDetailsComponent } from './permit-history-details/permit-history-details.component';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    PermitHistoriesComponent,
    PermitHistoryDetailsComponent
  ],
  imports: [
    CommonModule,
    PermitHistoryRoutingModule,
    MatProgressBarModule,
    MatTableModule,
    MatIconModule,
    MatDividerModule,
    MatPaginatorModule,
    MatButtonModule
  ]
})
export class PermitHistoryModule { }
