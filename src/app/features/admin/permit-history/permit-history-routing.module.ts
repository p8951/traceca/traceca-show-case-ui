import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {PermitHistoriesComponent} from '@admin/permit-history/permit-histories/permit-histories.component';
import {
  PermitHistoryDetailsComponent
} from '@admin/permit-history/permit-history-details/permit-history-details.component';

const routes: Routes = [
  {
    path: '',
    component: PermitHistoriesComponent
  },
  {
    path: ':id',
    component: PermitHistoryDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PermitHistoryRoutingModule { }
