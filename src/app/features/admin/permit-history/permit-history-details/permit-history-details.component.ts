import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ITransportPermit} from '@shared/models/transport-permit';
import {TransportPermitStore} from '@shared/stores/transport-permit.store';
import {Observable, Subject, Subscription, takeUntil} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {UserApiService} from '@shared/api/user-api.service';
import {IUser} from '@shared/models/user';

@Component({
  selector: 'app-permit-history-details',
  templateUrl: './permit-history-details.component.html',
  styleUrls: ['./permit-history-details.component.scss']
})
export class PermitHistoryDetailsComponent implements OnInit, OnDestroy {

  user: IUser | undefined;
  transportPermit$: Observable<ITransportPermit | null> | undefined ;

  private unsubscribe = new Subject();
  constructor(private store: TransportPermitStore, private activeRoute: ActivatedRoute,
              private router: Router, private userApi: UserApiService) {
  }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    const id = this.activeRoute.snapshot.params['id'];
    this.transportPermit$ = this.store.getDataById$(id);
  }


  back(): void {
    this.router.navigate(['../'], {relativeTo: this.activeRoute}).then();
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
