import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermitHistoryDetailsComponent } from './permit-history-details.component';

describe('PermitHistoryDetailsComponent', () => {
  let component: PermitHistoryDetailsComponent;
  let fixture: ComponentFixture<PermitHistoryDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermitHistoryDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PermitHistoryDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
