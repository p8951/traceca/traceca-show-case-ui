import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermitHistoriesComponent } from './permit-histories.component';

describe('PermitHistoriesComponent', () => {
  let component: PermitHistoriesComponent;
  let fixture: ComponentFixture<PermitHistoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermitHistoriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PermitHistoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
