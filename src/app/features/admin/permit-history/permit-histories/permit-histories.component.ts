import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {ITransportPermit, ITransportPermitHistoryFilter} from '@shared/models/transport-permit';
import {TransportPermitApiService} from '@shared/api/transport-permit-api.service';
import {TransportPermitStore} from '@shared/stores/transport-permit.store';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-permit-histories',
  templateUrl: './permit-histories.component.html',
  styleUrls: ['./permit-histories.component.scss']
})
export class PermitHistoriesComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ITransportPermit> = new MatTableDataSource();
  displayedColumns: string[] = ['idPermits', 'countryA', 'countryB', 'transitCountry', 'status', 'createDate', 'carrier', 'actions'];
  filter: ITransportPermitHistoryFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private transportPermitApi: TransportPermitApiService,
              private transportPermitStore: TransportPermitStore,
              private router: Router, private activeRoute: ActivatedRoute) {
    this.load();
  }

  ngOnInit(): void {

  }

  load(): void {
    this.isLoading = true;
    this.transportPermitApi.getHistoryPagedList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.transportPermitStore.setData(responseData.items);
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  openDialogDetails(id: number): void {
    this.router.navigate([id], {relativeTo: this.activeRoute}).then();
  }

  getStatusName(value: boolean): string {
    if (value) {
      return 'Used';
    } else {
      return 'New';
    }
  }
  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
