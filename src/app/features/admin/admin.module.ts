import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { LayoutComponent } from './layout/layout.component';
import {LeftMenuComponent} from '@admin/layout/left-menu/left-menu.component';
import {MatIconModule} from '@angular/material/icon';
import {StoresModule} from '@shared/stores/stores.module';
import { AdditionalPermissionComponent } from './additional-permission/additional-permission.component';
import {MatButtonModule} from '@angular/material/button';
import {MatMenuModule} from '@angular/material/menu';


@NgModule({
  declarations: [
    LayoutComponent, LeftMenuComponent, AdditionalPermissionComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    MatIconModule, StoresModule, MatButtonModule, MatMenuModule
  ]
})
export class AdminModule { }
