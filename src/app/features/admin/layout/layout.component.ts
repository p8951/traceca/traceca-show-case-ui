import {Component, OnDestroy, OnInit} from '@angular/core';
import {ILeftMenu} from '@admin/layout/left-menu/left-menu';
import {ICountry, ICountryFilter} from '@shared/models/country';
import {Subject, takeUntil} from 'rxjs';
import {CountryApiService} from '@shared/api/country-api.service';
import {CountryStore} from '@shared/stores/country.store';
import {AuthenticationService} from '@coreDirectory/auth/authentication.service';
import {AuthUser} from '@coreDirectory/auth/auth-user';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {

  menuData: ILeftMenu = {
    title: 'Меню',
    menus: [
      {
        icon: 'manage_accounts',
        url: 'user-register',
        title: 'User Register'
      },
      {
        icon: 'fact_check',
        url: 'couple-permit',
        title: 'Create permit'
      },
      {
        icon: 'fact_check',
        url: 'permit',
        title: 'Created permits'
      },
      {
        icon: 'fact_check',
        url: 'country',
        title: 'Countries'
      },
      {
        icon: 'face',
        url: 'user',
        title: 'Users'
      }
    ]
  };
  private unsubscribe = new Subject();
  user: AuthUser;
  userCountry: ICountry | undefined;

  constructor(private countryApi: CountryApiService,
              private countryStore: CountryStore,
              private authService: AuthenticationService) {
    this.loadCountries();
    this.user = authService.userValue;
  }

  ngOnInit(): void {
  }

  loadCountries(): void {
    const countryFilter: ICountryFilter = {pageNumber: 1, pageSize: 260};
    this.countryApi.getList(countryFilter).pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      if (responseData) {
        this.countryStore.setData(responseData);
        if (this.user.countryId) {
          this.countryStore.getDataById$(this.user.countryId).pipe(takeUntil(this.unsubscribe))
            .subscribe(data => {
              if (data)
                this.userCountry = data
            });
        }
      }
    })
  }

  logout(): void {
    this.authService.logout();
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
