export interface ILeftMenu {
  title?: string;
  menus: IMenuItem[];
}

export interface IMenuItem {
  title: string;
  url: string;
  icon: string;
}
