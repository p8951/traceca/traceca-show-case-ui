import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TransitCountryApiService} from '@shared/api/transit-country-api.service';
import {Subject, takeUntil} from 'rxjs';
import {ITransitCountry, ITransitCountryFilter} from '@shared/models/transit-country';
import {ICountry} from '@shared/models/country';
import {CountryStore} from '@shared/stores/country.store';

@Component({
  selector: 'app-transit-country-add',
  templateUrl: './transit-country-add.component.html',
  styleUrls: ['./transit-country-add.component.scss']
})
export class TransitCountryAddComponent implements OnInit, OnDestroy {

  private unsubscribe = new Subject();
  createForm: FormGroup;
  transitCountries: ITransitCountry[] = [];
  countries: ICountry[] = [];

  constructor(public dialogRef: MatDialogRef<TransitCountryAddComponent>,
              @Inject(MAT_DIALOG_DATA) public data: number,
              private formBuilder: FormBuilder,
              private transitCountryApi: TransitCountryApiService,
              private countryStore: CountryStore) {
    this.createForm = this.formBuilder.group({
      countPermit: [1, Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadCountries();
  }

  load(): void {
    const request: ITransitCountryFilter = {
      countryCoupleYearPermitCountId: this.data
    };
    this.transitCountryApi.getList(request).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        if (responseData) {
          this.transitCountries = responseData;
          this.transitCountries.forEach(x=> {
            x.countryId = x.country.id;
            x.countryCoupleYearPermitCountId = x.countryCoupleYearPermitCount.id
          });
        }
      });
  }

  removeData(id: number): void {
  const index = this.transitCountries.findIndex(c => c.countryId === id);
    this.transitCountries.splice(index, 1);
  }

  loadCountries(): void {
    this.countryStore.getData$().pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        if (responseData) {
          this.countries = responseData;
          this.load();
        }
      });
  }

  get f() {
    return this.createForm.controls;
  }

  submit(): void {
    this.dialogRef.close(this.transitCountries);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  add(): void {
    const newTransit: ICountry = {
      id: 0,
      name: {ru: '', en: ''},
      iso: 0,
      alpha2: '',
      alpha3: '',
      createdAt: new Date()
    };
    // @ts-ignore
    this.transitCountries.push(newTransit)
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
