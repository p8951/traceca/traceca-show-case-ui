import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransitCountryAddComponent } from './transit-country-add.component';

describe('TransitCountryAddComponent', () => {
  let component: TransitCountryAddComponent;
  let fixture: ComponentFixture<TransitCountryAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TransitCountryAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransitCountryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
