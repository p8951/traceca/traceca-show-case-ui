import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCoupleYearPermitCountsComponent } from './country-couple-year-permit-counts.component';

describe('CountryCoupleYearPermitCountsComponent', () => {
  let component: CountryCoupleYearPermitCountsComponent;
  let fixture: ComponentFixture<CountryCoupleYearPermitCountsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryCoupleYearPermitCountsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCoupleYearPermitCountsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
