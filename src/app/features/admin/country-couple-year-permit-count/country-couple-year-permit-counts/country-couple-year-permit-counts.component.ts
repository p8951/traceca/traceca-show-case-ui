import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Subject, takeUntil} from 'rxjs';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {
  ICountryCoupleYearPermitCount,
  ICountryCoupleYearPermitCountFilter
} from '@shared/models/country-couple-year-permit-count';
import {CountryCoupleYearPermitCountApiService} from '@shared/api/country-couple-year-permit-count-api.service';
import {MatDialog} from '@angular/material/dialog';
import {
  CountryCoupleYearPermitCountCreateComponent
} from '@admin/country-couple-year-permit-count/country-couple-year-permit-count-create/country-couple-year-permit-count-create.component';
import {
  CountryCoupleYearPermitCountAddCountComponent
} from '@admin/country-couple-year-permit-count/country-couple-year-permit-count-add-count/country-couple-year-permit-count-add-count.component';
import {
  TransitCountryAddComponent
} from '@admin/country-couple-year-permit-count/transit-country-add/transit-country-add.component';
import {ITransitCountry, ITransitCountryCreate} from '@shared/models/transit-country';
import {TransitCountryApiService} from '@shared/api/transit-country-api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-country-couple-year-permit-counts',
  templateUrl: './country-couple-year-permit-counts.component.html',
  styleUrls: ['./country-couple-year-permit-counts.component.scss']
})
export class CountryCoupleYearPermitCountsComponent implements OnInit, OnDestroy {

  dataSource: MatTableDataSource<ICountryCoupleYearPermitCount> = new MatTableDataSource();
  displayedColumns: string[] = ['id', 'year', 'firstCountry', 'secondCountry', 'permitCount', 'actions'];
  filter: ICountryCoupleYearPermitCountFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private yearPermitApi: CountryCoupleYearPermitCountApiService,
              public dialog: MatDialog,
              private transitCountryApi: TransitCountryApiService,
              private router: Router, private route: ActivatedRoute) {
  }


  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.yearPermitApi.getPagedList(this.filter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage - 1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.filter.pageSize = event.pageSize;
    this.filter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  openDialog(): void {
    // const dialogRef = this.dialog.open(CountryCoupleYearPermitCountCreateComponent, {
    //   width: '500px'
    // });
    //
    // dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
    //   if (result) {
    //     this.yearPermitApi.create(result).pipe(takeUntil(this.unsubscribe)).subscribe(
    //       responseData => {
    //         this.load();
    //       }
    //     );
    //   }
    // });
    this.router.navigate(['create'],
      {relativeTo: this.route}).then();
  }

  openDialogAdd(id: number): void {
    const dialogRef = this.dialog.open(CountryCoupleYearPermitCountAddCountComponent, {
      width: '250px'
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result) {
        this.yearPermitApi.addPermitCount(id, result).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  openDialogTransitCountryAdd(id: number): void {
    const dialogRef = this.dialog.open(TransitCountryAddComponent, {
      width: '400px',
      data: id,
    });

    dialogRef.beforeClosed().pipe(takeUntil(this.unsubscribe)).subscribe((result: ITransitCountry[])  => {
      if (result) {
        this.transitCountryApi.deleteDataByCountryCoupleId(id).pipe(takeUntil(this.unsubscribe))
          .subscribe( dt => {
            for (let i=0; i < result.length; i++) {
              const request: ITransitCountryCreate = {
                countryCoupleYearPermitCountId: id,
                countryId: result[i].country.id
              }
              this.transitCountryApi.create(request).pipe(takeUntil(this.unsubscribe)).subscribe(
                responseData => {
                }
              );

            }
          });


      }
    });
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
