import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCoupleYearPermitCountAddCountComponent } from './country-couple-year-permit-count-add-count.component';

describe('CountryCoupleYearPermitCountAddCountComponent', () => {
  let component: CountryCoupleYearPermitCountAddCountComponent;
  let fixture: ComponentFixture<CountryCoupleYearPermitCountAddCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryCoupleYearPermitCountAddCountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCoupleYearPermitCountAddCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
