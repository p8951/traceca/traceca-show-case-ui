import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-country-couple-year-permit-count-add-count',
  templateUrl: './country-couple-year-permit-count-add-count.component.html',
  styleUrls: ['./country-couple-year-permit-count-add-count.component.scss']
})
export class CountryCoupleYearPermitCountAddCountComponent implements OnInit {

  createForm: FormGroup;
  constructor(public dialogRef: MatDialogRef<CountryCoupleYearPermitCountAddCountComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string,
              private formBuilder: FormBuilder) {
    this.createForm = this.formBuilder.group({
      countPermit: [1, Validators.required]
    });
  }

  ngOnInit(): void {
  }

  get f() {
    return this.createForm.controls;
  }

  submit(): void {
    if (this.createForm.invalid) {
      return;
    }
    const countPermit = this.f['countPermit'].value;
    this.dialogRef.close(countPermit);
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

}
