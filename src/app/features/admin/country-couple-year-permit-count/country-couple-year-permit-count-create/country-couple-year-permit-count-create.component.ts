import {Component, OnDestroy, OnInit} from '@angular/core';
import {ICountry} from '@shared/models/country';
import {Subject, takeUntil} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CountryStore} from '@shared/stores/country.store';
import {CountryCoupleYearPermitCountApiService} from '@shared/api/country-couple-year-permit-count-api.service';
import {IAdditionalPermission} from '@shared/models/additional-permission';
import {RoleEnum} from '@shared/enums/role.enum';
import {ICountryCoupleYearPermitCountCreate} from '@shared/models/country-couple-year-permit-count';
import {Router} from '@angular/router';

@Component({
  selector: 'app-country-couple-year-permit-count-create',
  templateUrl: './country-couple-year-permit-count-create.component.html',
  styleUrls: ['./country-couple-year-permit-count-create.component.scss']
})
export class CountryCoupleYearPermitCountCreateComponent implements OnInit, OnDestroy {

  createForm: FormGroup;
  countries: ICountry[] = [];
  transitCountryIds: ICountry[] = [];
  private unsubscribe = new Subject();
  additionalPermissions: IAdditionalPermission[] = []
  roles = {
    admin: RoleEnum.Admin,
    carrier: RoleEnum.Carrier,
    traceca: RoleEnum.Traceca,

  };
  loading = false;

  constructor(private countryStore: CountryStore,
              private yearPermitApi: CountryCoupleYearPermitCountApiService,
              private formBuilder: FormBuilder, private router: Router
              ) {
    this.createForm = this.formBuilder.group({
      firstCountryId: ['', Validators.required],
      secondCountryId: ['', Validators.required],
      countPermit: [1, Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadCountries();
  }

  loadCountries(): void {
    this.countryStore.getData$().pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      if (responseData) {
        this.countries = responseData
      }
    })
  }

  get f() {
    return this.createForm.controls;
  }

  submit(): void {
    if (this.createForm.invalid) {
      return;
    }

    const request: ICountryCoupleYearPermitCountCreate = {
      firstCountryId: this.f['firstCountryId'].value,
      secondCountryId: this.f['secondCountryId'].value,
      countPermit: this.f['countPermit'].value,
      additionalFields: this.additionalPermissions,
      transitCountries: this.transitCountryIds.map(v => v.id)
    };
    this.yearPermitApi.create(request).pipe(takeUntil(this.unsubscribe)).subscribe(responseData => {
      this.createForm.reset();
      this.transitCountryIds = [];
      this.additionalPermissions = [];
    });

  }

  removeData(id: number): void {
    const index = this.transitCountryIds.findIndex(c => c.id === id);
    this.transitCountryIds.splice(index, 1);
  }

  add(): void {
    this.transitCountryIds.push({
      id: 0,
      iso: 0,
      alpha3: '',
      alpha2: '',
      name: {en: '', ru: ''},
      createdAt: new Date()
    });
  }

  addAdditional(): void {
    this.additionalPermissions.push({
      role: RoleEnum.Traceca,
      fieldName: '',
      isActive: true
    });
  }

  removeAdditional(value: IAdditionalPermission): void {
    const index = this.additionalPermissions.findIndex(c => c.role === value.role && c.fieldName === value.fieldName);
    this.additionalPermissions.splice(index, 1);
  }

  back(): void {
    this.router.navigate(['/admin/couple-permit']).then();
  }

  ngOnDestroy() {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
