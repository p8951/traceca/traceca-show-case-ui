import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryCoupleYearPermitCountCreateComponent } from './country-couple-year-permit-count-create.component';

describe('CountryCoupleYearPermitCountCreateComponent', () => {
  let component: CountryCoupleYearPermitCountCreateComponent;
  let fixture: ComponentFixture<CountryCoupleYearPermitCountCreateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CountryCoupleYearPermitCountCreateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryCoupleYearPermitCountCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
