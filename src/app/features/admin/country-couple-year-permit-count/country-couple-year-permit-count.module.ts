import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CountryCoupleYearPermitCountRoutingModule } from './country-couple-year-permit-count-routing.module';
import { CountryCoupleYearPermitCountsComponent } from './country-couple-year-permit-counts/country-couple-year-permit-counts.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { CountryCoupleYearPermitCountCreateComponent } from './country-couple-year-permit-count-create/country-couple-year-permit-count-create.component';
import {MatCardModule} from '@angular/material/card';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatDialogModule} from '@angular/material/dialog';
import { CountryCoupleYearPermitCountAddCountComponent } from './country-couple-year-permit-count-add-count/country-couple-year-permit-count-add-count.component';
import { TransitCountryAddComponent } from './transit-country-add/transit-country-add.component';


@NgModule({
  declarations: [
    CountryCoupleYearPermitCountsComponent,
    CountryCoupleYearPermitCountCreateComponent,
    CountryCoupleYearPermitCountAddCountComponent,
    TransitCountryAddComponent
  ],
    imports: [
        CommonModule,
        CountryCoupleYearPermitCountRoutingModule,
        MatFormFieldModule,
        MatProgressBarModule,
        MatTableModule,
        MatDividerModule,
        MatPaginatorModule,
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatCardModule,
        ReactiveFormsModule,
        MatSelectModule,
        MatDialogModule,
        FormsModule
    ]
})
export class CountryCoupleYearPermitCountModule { }
