import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  CountryCoupleYearPermitCountsComponent
} from '@admin/country-couple-year-permit-count/country-couple-year-permit-counts/country-couple-year-permit-counts.component';
import {
  CountryCoupleYearPermitCountCreateComponent
} from '@admin/country-couple-year-permit-count/country-couple-year-permit-count-create/country-couple-year-permit-count-create.component';

const routes: Routes = [
  {
    path: '',
    component: CountryCoupleYearPermitCountsComponent
  },
  {
    path: 'create',
    component: CountryCoupleYearPermitCountCreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CountryCoupleYearPermitCountRoutingModule { }
