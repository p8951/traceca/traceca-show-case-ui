import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserRegistersComponent} from '@admin/user-register/user-registers/user-registers.component';

const routes: Routes = [
  {
    path: '',
    component: UserRegistersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRegisterRoutingModule { }
