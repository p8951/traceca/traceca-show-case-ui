import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {IUserRegister, IUserRegisterFilter} from '@shared/models/user-register';
import {UserRegisterApiService} from '@shared/api/user-register-api.service';
import {Subject, takeUntil} from 'rxjs';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatDialog} from '@angular/material/dialog';
import {
  UserRegisterActivateConfirmComponent
} from '@admin/user-register/user-register-activate-confirm/user-register-activate-confirm.component';
import {RoleEnum} from '@shared/enums/role.enum';

@Component({
  selector: 'app-user-registers',
  templateUrl: './user-registers.component.html',
  styleUrls: ['./user-registers.component.scss']
})
export class UserRegistersComponent implements OnInit, OnDestroy {

  userRegisters: IUserRegister[] = [];
  dataSource: MatTableDataSource<IUserRegister> = new MatTableDataSource();
  displayedColumns: string[] = ['fullName', 'status', 'userName', 'email', 'role', 'country', 'organization', 'contacts',
                                'actions'];
  userFilter: IUserRegisterFilter = {
    pageNumber: 1,
    pageSize: 10
  };
  totalCount = 0;
  isLoading = false;
  private unsubscribe = new Subject();

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  constructor(private userRegisterApi: UserRegisterApiService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.load();
  }

  load(): void {
    this.isLoading = true;
    this.userRegisterApi.getPagedList(this.userFilter).pipe(takeUntil(this.unsubscribe))
      .subscribe(responseData => {
        this.dataSource.data = responseData.items;
        this.totalCount = responseData.totalCount;
        this.paginator.pageIndex = responseData.currentPage -1;
        this.paginator.length = responseData.totalCount;
        this.isLoading = false;
      });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  pageChanged(event: PageEvent) {
    this.userFilter.pageSize = event.pageSize;
    this.userFilter.pageNumber = event.pageIndex + 1;
    this.load();
  }

  openDialog(id: string): void {
    const dialogRef = this.dialog.open(UserRegisterActivateConfirmComponent, {
      width: '250px',
      data: id,
    });

    dialogRef.afterClosed().pipe(takeUntil(this.unsubscribe)).subscribe(result => {
      if (result === 'ok') {
        this.userRegisterApi.activate(id).pipe(takeUntil(this.unsubscribe)).subscribe(
          responseData => {
            this.load();
          }
        );
      }
    });
  }

  activatedName(value: boolean): string {
    if (value) {
      return 'Activated'
    } else {
      return 'Not activated'
    }
  }

  roleName(value: RoleEnum): string {
    switch (value) {
      case RoleEnum.Admin: return  'Admin'
      case RoleEnum.Traceca: return  'Traceca'
      case RoleEnum.Carrier: return  'Carrier'
      default: return ''
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe.next(null);
    this.unsubscribe.complete();
  }
}
