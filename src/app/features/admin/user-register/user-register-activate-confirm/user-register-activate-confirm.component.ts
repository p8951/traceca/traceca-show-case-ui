import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-user-register-activate-confirm',
  templateUrl: './user-register-activate-confirm.component.html',
  styleUrls: ['./user-register-activate-confirm.component.scss']
})
export class UserRegisterActivateConfirmComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<UserRegisterActivateConfirmComponent>,
              @Inject(MAT_DIALOG_DATA) public data: string) { }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
