import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserRegisterActivateConfirmComponent } from './user-register-activate-confirm.component';

describe('UserRegisterActivateConfirmComponent', () => {
  let component: UserRegisterActivateConfirmComponent;
  let fixture: ComponentFixture<UserRegisterActivateConfirmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserRegisterActivateConfirmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRegisterActivateConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
