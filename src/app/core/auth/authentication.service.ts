import {Injectable} from '@angular/core';
import {BehaviorSubject, map, Observable} from 'rxjs';
import {AuthUser} from './auth-user';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private userSubject: BehaviorSubject<AuthUser>;
  public user: Observable<AuthUser>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    // @ts-ignore
    this.userSubject = new BehaviorSubject<AuthUser>(null);
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): AuthUser {
    return this.userSubject.value;
  }

  setUserValue(): void {
    const userStr = localStorage.getItem('userInfo');
    if (userStr) {
      const user: AuthUser = JSON.parse(userStr);
      this.userSubject.next(user);
    }
  }

  login(username: string, password: string) {
    return this.http.post<any>(`/api/Account/authenticate`, {
      username,
      password
    }, {withCredentials: true})
      .pipe(map(user => {
        localStorage.setItem('userInfo', JSON.stringify(user));
        this.userSubject.next(user);
        this.startRefreshTokenTimer();
        return user;
      }));
  }

  logout() {
    this.http.post<any>(`/api/Account/revoke-token`, {}, {withCredentials: true}).subscribe();
    this.stopRefreshTokenTimer();
    // @ts-ignore
    this.userSubject.next(null);
    localStorage.removeItem('userInfo')
    this.router.navigate(['/login']).then();
  }

  refreshToken() {
    return this.http.post<any>(`/api/Account/refresh-token`, {}, {withCredentials: true})
      .pipe(map((user) => {
        // this.userSubject.next(user);
        localStorage.setItem('userInfo', JSON.stringify(user));
        this.startRefreshTokenTimer();
        return user;
      }));
  }

  // helper methods

  private refreshTokenTimeout: any;

  private startRefreshTokenTimer() {
    if (!this.userValue.jwtToken) {
      return;
    }

    // parse json object from base64 encoded jwt token
    const jwtToken = JSON.parse(atob(this.userValue.jwtToken.split('.')[1]));

    // set a timeout to refresh the token a minute before it expires
    const expires = new Date(jwtToken.exp * 1000);
    const timeout = expires.getTime() - Date.now() - (60 * 1000);
    this.refreshTokenTimeout = setTimeout(() => this.refreshToken().subscribe(), timeout);
  }

  private stopRefreshTokenTimer() {
    clearTimeout(this.refreshTokenTimeout);
  }
}
