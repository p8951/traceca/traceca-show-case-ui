import {RoleEnum} from '@shared/enums/role.enum';

export class AuthUser {
  id?: number;
  username?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  jwtToken?: string;
  countryId?: number;
  roles?: RoleDto[];
}

export class RoleDto {
  id?: RoleEnum;
  code?: string;
}
