export interface ITranslationName {
  en: string;
  ru: string;
}
