export interface IPagedList<T> {
  currentPage: number;
  totalPages: number;
  pageSize: number;
  totalCount: number;
  hasPrevious: boolean;
  hasNext: boolean;
  items: T[];
}

export interface IPagedRequest {
  pageNumber: number;
  pageSize: number;
}
