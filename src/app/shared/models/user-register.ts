import {RoleEnum} from '@shared/enums/role.enum';
import {IPagedList, IPagedRequest} from '@shared/models/base/paged-list';
import {ICountry} from '@shared/models/country';

export interface IUserRegister {
  id: string;
  lastName: string;
  firstName: string;
  patronymic: string;
  userName: string;
  password: string;
  email: string;
  role: RoleEnum;
  countryId: number;
  country: ICountry;
  organization: string;
  contacts: string;
  activated: boolean
}

export interface IUserRegisterCreate {
  lastName: string;
  firstName: string;
  patronymic: string;
  userName: string;
  password: string;
  email: string;
  role: RoleEnum;
  countryId: number;
  contacts: string;
  organization?: string;
  address?: string;
  city?: string;
}

export interface IUserRegisterFilter extends IPagedRequest {

}
