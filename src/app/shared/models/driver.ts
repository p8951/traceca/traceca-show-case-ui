import {IPagedRequest} from '@shared/models/base/paged-list';

export interface IDriver {
  userId: number;
  lastName: string;
  firstName: string;
  username: string;
  email: string;
  contacts: string;
  isActive: boolean;
}

export interface IDriverCreate {
  lastName: string;
  firstName: string;
  username: string;
  email: string;
  contacts: string;
  password: string;
}

export interface IDriverFilter extends IPagedRequest{
  carrierId?: number;
}
