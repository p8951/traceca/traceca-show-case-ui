import {ITranslationName} from '@shared/models/base/translation-name';
import {IPagedRequest} from '@shared/models/base/paged-list';

export interface ICountry {
  id: number;
  name: ITranslationName;
  alpha2: string;
  alpha3: string;
  iso: number;
  createdAt: Date;
}

export interface ICountryFilter extends IPagedRequest{
  name?: string
}
