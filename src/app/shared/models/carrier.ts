import {ITranslationName} from '@shared/models/base/translation-name';
import {ICountry} from '@shared/models/country';
import {IPagedRequest} from '@shared/models/base/paged-list';
import {IAdditionalFieldValue} from '@shared/models/additional-permission';

export interface ICarrier {
  id: number;
  name: ITranslationName;
  countryId: number;
  country: ICountry;
  organization: string;
  contacts: string;
  email?: string;
  city?: string;
  address?: string;
  additionalFieldValues?: IAdditionalFieldValue[];
}

export interface ICarrierFilter extends IPagedRequest {

}
