import {ICarrier} from '@shared/models/carrier';
import {IPagedRequest} from '@shared/models/base/paged-list';
import {IAdditionalFieldValue} from '@shared/models/additional-permission';
import {IUser} from '@shared/models/user';

export interface ITransportCreate {
  carrierId?: number;
  governmentNumber: string;
  driver: string;
  model: string;
  userId?: number;
}

export interface ITransport {
  id: number;
  carrierId: number;
  carrier: ICarrier;
  governmentNumber: string;
  driver: string;
  model: string;
  additionalFieldValues: IAdditionalFieldValue[];
  user: IUser;
}
export interface ITransportFilter extends IPagedRequest {

}
