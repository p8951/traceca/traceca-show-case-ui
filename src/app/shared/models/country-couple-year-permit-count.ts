import {ICountry} from '@shared/models/country';
import {IPagedRequest} from '@shared/models/base/paged-list';
import {IAdditionalPermission} from '@shared/models/additional-permission';

export interface ICountryCoupleYearPermitCountCreate {
  firstCountryId: number;
  secondCountryId: number;
  countPermit: number;
  transitCountries: number[];
  additionalFields: IAdditionalPermission[];
}

export interface ICountryCoupleYearPermitCount {
  id: number;
  firstCountryId: number;
  firstCountry: ICountry;
  secondCountryId: number;
  secondCountry: ICountry;
  year: number;
  countPermit: number;
  createdAt: Date;
}

export interface ICountryCoupleYearPermitCountFilter extends IPagedRequest{
  year?: number;
  firstCountryId?: number;
}
