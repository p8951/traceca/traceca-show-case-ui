import {RoleEnum} from '@shared/enums/role.enum';
import {ICountry} from '@shared/models/country';

export interface IUser {
  id: number;
  lastName: string;
  firstName: string;
  patronymic: string;
  fullName: string;
  userName: string;
  email: string;
  role: RoleEnum[];
  countryId: number;
  country: ICountry;
  isActive: boolean
}
