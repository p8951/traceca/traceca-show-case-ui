import {ICountryCoupleYearPermitCount} from '@shared/models/country-couple-year-permit-count';

export interface IPermit {
  id: number;
  permitCode: string;
  used: boolean;
  countryCoupleYearPermitCount: ICountryCoupleYearPermitCount;
}
