import {ITransport} from '@shared/models/transport';
import {IPermit} from '@shared/models/permit';
import {IPagedRequest} from '@shared/models/base/paged-list';

export interface ITransportPermit {
  id: number;
  transportId: number;
  transport: ITransport;
  permitId: number;
  permit: IPermit;
  used: boolean;
  createdAt: Date;
}

export interface ITransportPermitCreate {
  transportId: number;
  countryCoupleYearPermitCountId: number;
}

export interface ITransportPermitFilter extends IPagedRequest {

}

export interface ITransportPermitHistoryFilter extends IPagedRequest {
  permitCode?: string;
  firstCountryId?: number;
  secondCountryId?: number;
}
