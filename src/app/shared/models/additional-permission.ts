import {RoleEnum} from '@shared/enums/role.enum';

export interface IAdditionalPermission {
  fieldName: string;
  isActive: boolean;
  role: RoleEnum;
}

export interface IAdditionalFieldValue {
  fieldName: string;
  fieldValue?: string;
  isActive?: boolean;
}
