import {ICountryCoupleYearPermitCount} from '@shared/models/country-couple-year-permit-count';
import {ICountry} from '@shared/models/country';

export interface ITransitCountryCreate {
  countryCoupleYearPermitCountId: number;
  countryId: number;
}

export interface ITransitCountry {
  countryCoupleYearPermitCountId?: number;
  countryCoupleYearPermitCount: ICountryCoupleYearPermitCount;
  countryId: number;
  country: ICountry;
}

export interface ITransitCountryFilter {
  countryCoupleYearPermitCountId?: number;
}
