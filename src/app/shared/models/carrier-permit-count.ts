import {ICarrier} from '@shared/models/carrier';
import {ICountryCouplePermitCount} from '@shared/models/country-couple-permit-count';
import {IPagedRequest} from '@shared/models/base/paged-list';

export interface ICarrierPermitCountCreate {
  carrierId: number;
  countryCouplePermitCountId: number;
  permitCount: number;
}

export interface ICarrierPermitCount {
  carrierId: number;
  carrier: ICarrier;
  countryCouplePermitCountId: number;
  countryCouplePermitCount: ICountryCouplePermitCount;
  permitCount: number;
}

export interface ICarrierPermitCountFilter extends IPagedRequest {
  countryCouplePermitCountId?: number;
}
