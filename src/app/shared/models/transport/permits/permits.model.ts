export class PermitsModel {
  id?: number;
  countryB?: string;
  transitCountry?: string;
  date?: Date = new Date();
}
