export class NewsModel {
  title?: string;
  date?: Date = new Date();
  description?: string;
}
