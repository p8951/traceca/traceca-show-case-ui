import {ICountryCoupleYearPermitCount} from '@shared/models/country-couple-year-permit-count';
import {IPagedRequest} from '@shared/models/base/paged-list';

export interface ICountryCouplePermitCount {
  id: number;
  countryCoupleYearPermitCountId: number;
  countryCoupleYearPermitCount: ICountryCoupleYearPermitCount;
  permitCount: number;
}

export interface ICountryCouplePermitCountCreate {
  countryCoupleYearPermitCountId: number;
  permitCount: number;
}

export interface ICountryCouplePermitCountFilter extends IPagedRequest{
  secondCountryId?: number;
}
