import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {
  ICountryCouplePermitCount,
  ICountryCouplePermitCountCreate,
  ICountryCouplePermitCountFilter
} from '@shared/models/country-couple-permit-count';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
import {ITransitCountry, ITransitCountryCreate, ITransitCountryFilter} from '@shared/models/transit-country';

@Injectable({
  providedIn: 'root'
})
export class TransitCountryApiService extends BaseApiService {

  create(request: ITransitCountryCreate): Observable<number> {
    return this.postData('/api/TransitCountry', request);
  }

  getPagedList(request: ITransitCountryFilter): Observable<IPagedList<ITransitCountry>> {
    return this.getData('/api/TransitCountry/Paged', request);
  }

  getList(request: ITransitCountryFilter): Observable<ITransitCountry[]> {
    return this.getData('/api/TransitCountry', request);
  }

  deleteDataByCountryCoupleId(id: number): Observable<null> {
    return this.removeData('/api/TransitCountry/CountryCouple/' + id, {});
  }



}
