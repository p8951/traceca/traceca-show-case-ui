import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IUser} from '@shared/models/user';

@Injectable({
  providedIn: 'root'
})
export class UserApiService extends BaseApiService {

  getById(id: number): Observable<IUser> {
    return this.getData('/api/User/' + id, {});
  }
}
