import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {ICountry, ICountryFilter} from '@shared/models/country';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
@Injectable({
  providedIn: 'root'
})
export class CountryApiService extends BaseApiService {
  getPagedList(request: ICountryFilter): Observable<IPagedList<ICountry>> {
    return this.getData('/api/Country/Paged', request);
  }

  getList(request: ICountryFilter): Observable<ICountry[]> {
    return this.getData('/api/Country', request);
  }


}
