import {Injectable} from '@angular/core';
import {BaseApiService} from '@shared/api/base-api.service';

import {Observable} from 'rxjs';
import {IPagedList, IPagedRequest} from '@shared/models/base/paged-list';
import {
  ICountryCouplePermitCount,
  ICountryCouplePermitCountCreate,
  ICountryCouplePermitCountFilter
} from '@shared/models/country-couple-permit-count';

@Injectable({
  providedIn: 'root'
})
export class CountryCouplePermitCountApiService extends BaseApiService {

  create(request: ICountryCouplePermitCountCreate): Observable<number> {
    return this.postData('/api/CountryCouplePermitCount', request);
  }

  getPagedList(request: ICountryCouplePermitCountFilter): Observable<IPagedList<ICountryCouplePermitCount>> {
    return this.getData('/api/CountryCouplePermitCount/Paged', request);
  }

  getPagedCurrentBList(request: IPagedRequest): Observable<IPagedList<ICountryCouplePermitCount>> {
    return this.getData('/api/CountryCouplePermitCount/PagedCurrentB', request);
  }

  getPagedCurrentAList(request: IPagedRequest): Observable<IPagedList<ICountryCouplePermitCount>> {
    return this.getData('/api/CountryCouplePermitCount/PagedCurrentA', request);
  }

  getList(request: ICountryCouplePermitCountFilter): Observable<ICountryCouplePermitCount[]> {
    return this.getData('/api/CountryCouplePermitCount', request);
  }

  getCurrent(): Observable<ICountryCouplePermitCount[]> {
    return this.getData('/api/CountryCouplePermitCount/Current', {});
  }

  // getCurrentList(): Observable<ICountryCoupleYearPermitCount[]> {
  //   return this.getData('/api/CountryCoupleYearPermitCount/Current', {});
  // }

  addPermitCount(id: number, permitCount: number): Observable<number> {
    return this.putData('/api/CountryCouplePermitCount/' + id + '/AddPermitCount', {permitCount});
  }
}
