import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IPagedList, IPagedRequest} from '@shared/models/base/paged-list';
import {
  ICarrierPermitCount,
  ICarrierPermitCountCreate,
  ICarrierPermitCountFilter
} from '@shared/models/carrier-permit-count';

@Injectable({
  providedIn: 'root'
})
export class CarrierPermitCountApiService extends BaseApiService {

  create(request: ICarrierPermitCountCreate): Observable<number> {
    return this.postData('/api/CarrierPermitCount', request);
  }

  getPagedList(request: ICarrierPermitCountFilter): Observable<IPagedList<ICarrierPermitCount>> {
    return this.getData('/api/CarrierPermitCount/Paged', request);
  }

  getPagedCurrentList(request: IPagedRequest): Observable<IPagedList<ICarrierPermitCount>> {
    return this.getData('/api/CarrierPermitCount/PagedCurrent', request);
  }

  getList(request: ICarrierPermitCountFilter): Observable<ICarrierPermitCount[]> {
    return this.getData('/api/CarrierPermitCount', request);
  }

  addPermitCount(id: number, permitCount: number): Observable<number> {
    return this.putData('/api/CarrierPermitCount/' + id + '/AddPermitCount', {permitCount});
  }
}
