import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
import {IUserRegister, IUserRegisterCreate, IUserRegisterFilter} from '@shared/models/user-register';


@Injectable({
  providedIn: 'root'
})
export class UserRegisterApiService extends BaseApiService {

  create(request: IUserRegisterCreate): Observable<number> {
     return this.postData('/api/UserRegistration', request);
  }

  getPagedList(request: IUserRegisterFilter): Observable<IPagedList<IUserRegister>> {
    return this.getData('/api/UserRegistration', request);
  }

  activate(id: string): Observable<null> {
    return this.putData('/api/UserRegistration/' + id + '/activate', {});
  }
}
