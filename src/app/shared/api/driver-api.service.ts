import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {ITransport, ITransportCreate, ITransportFilter} from '@shared/models/transport';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
import {IDriver, IDriverCreate, IDriverFilter} from '@shared/models/driver';

@Injectable({
  providedIn: 'root'
})
export class DriverApiService extends BaseApiService {

  create(request: IDriverCreate): Observable<number> {
    return this.postData('/api/Driver', request);
  }

  getPagedList(request: IDriverFilter): Observable<IPagedList<IDriver>> {
    return this.getData('/api/Driver/Paged', request);
  }

  getList(request: IDriverFilter): Observable<IDriver[]> {
    return this.getData('/api/Driver', request);
  }

}
