import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {ICarrierPermitCount, ICarrierPermitCountFilter} from '@shared/models/carrier-permit-count';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
import {ICarrier, ICarrierFilter} from '@shared/models/carrier';

@Injectable({
  providedIn: 'root'
})
export class CarrierApiService extends BaseApiService{

  getPagedList(request: ICarrierFilter): Observable<IPagedList<ICarrier>> {
    return this.getData('/api/Carrier/Paged', request);
  }

  getPagedCurrentList(request: ICarrierFilter): Observable<IPagedList<ICarrier>> {
    return this.getData('/api/Carrier/PagedCurrent', request);
  }
}
