import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
import {
  ITransportPermit,
  ITransportPermitCreate,
  ITransportPermitFilter,
  ITransportPermitHistoryFilter
} from '@shared/models/transport-permit';

@Injectable({
  providedIn: 'root'
})
export class TransportPermitApiService extends BaseApiService {
  create(request: ITransportPermitCreate): Observable<number> {
    return this.postData('/api/TransportPermit', request);
  }

  getPagedList(request: ITransportPermitFilter): Observable<IPagedList<ITransportPermit>> {
    return this.getData('/api/TransportPermit/Paged', request);
  }

  getPagedCurrentList(request: ITransportPermitFilter): Observable<IPagedList<ITransportPermit>> {
    return this.getData('/api/TransportPermit/PagedCurrent', request);
  }

  getHistoryPagedList(request: ITransportPermitHistoryFilter): Observable<IPagedList<ITransportPermit>> {
    return this.getData('/api/TransportPermit/PagedHistory', request);
  }

  getCurrentUserPermitList(request: ITransportPermitFilter): Observable<IPagedList<ITransportPermit>> {
    return this.getData('/api/TransportPermit/CurrentUserPermitList', request);
  }

  getByPermitCode(permitCode: string): Observable<ITransportPermit> {
    return this.getData('/api/TransportPermit/ByPermitCode/' + permitCode, {});
  }
}
