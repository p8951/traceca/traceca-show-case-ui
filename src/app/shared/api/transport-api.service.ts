import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
import {ITransport, ITransportCreate, ITransportFilter} from '@shared/models/transport';
import {ICountryCouplePermitCountCreate} from '@shared/models/country-couple-permit-count';

@Injectable({
  providedIn: 'root'
})
export class TransportApiService extends BaseApiService {

  create(request: ITransportCreate): Observable<number> {
    return this.postData('/api/Transport', request);
  }

  getPagedList(request: ITransportFilter): Observable<IPagedList<ITransport>> {
    return this.getData('/api/Transport/Paged', request);
  }

  getPagedCurrentList(request: ITransportFilter): Observable<IPagedList<ITransport>> {
    return this.getData('/api/Transport/PagedCurrent', request);
  }
}
