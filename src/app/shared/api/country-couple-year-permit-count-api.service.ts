import {BaseApiService} from '@shared/api/base-api.service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IPagedList} from '@shared/models/base/paged-list';
import {
  ICountryCoupleYearPermitCount,
  ICountryCoupleYearPermitCountCreate,
  ICountryCoupleYearPermitCountFilter
} from '@shared/models/country-couple-year-permit-count';

@Injectable({
  providedIn: 'root'
})
export class CountryCoupleYearPermitCountApiService extends BaseApiService {

  create(request: ICountryCoupleYearPermitCountCreate): Observable<number> {
    return this.postData('/api/CountryCoupleYearPermitCount', request);
  }

  getPagedList(request: ICountryCoupleYearPermitCountFilter): Observable<IPagedList<ICountryCoupleYearPermitCount>> {
    return this.getData('/api/CountryCoupleYearPermitCount/Paged', request);
  }

  getList(request: ICountryCoupleYearPermitCountFilter): Observable<ICountryCoupleYearPermitCount[]> {
    return this.getData('/api/CountryCoupleYearPermitCount', request);
  }

  getCurrentList(): Observable<ICountryCoupleYearPermitCount[]> {
    return this.getData('/api/CountryCoupleYearPermitCount/Current', {});
  }

  addPermitCount(id: number, countPermit: number): Observable<number> {
    return this.putData('/api/CountryCoupleYearPermitCount/' + id + '/AddPermitCount', {countPermit});
  }
}
