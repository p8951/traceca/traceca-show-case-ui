import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {STORES} from '@shared/stores/index';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    STORES
  ]
})
export class StoresModule { }
