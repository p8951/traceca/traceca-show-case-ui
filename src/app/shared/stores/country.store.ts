import {BaseStore} from '@shared/stores/base.store';
import {ICountry} from '@shared/models/country';
import {Injectable} from '@angular/core';

@Injectable()
export class CountryStore extends BaseStore<ICountry> {

}
