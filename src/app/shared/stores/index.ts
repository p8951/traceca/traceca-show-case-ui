import {CountryStore} from '@shared/stores/country.store';
import {TransportPermitStore} from '@shared/stores/transport-permit.store';

export const STORES = [
  CountryStore, TransportPermitStore
];
