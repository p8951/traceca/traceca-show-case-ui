import {BehaviorSubject, Observable, of} from 'rxjs';
import {filter, map} from 'rxjs/operators';
import {Injectable} from '@angular/core';

interface BaseModel {
    id: number;
}
@Injectable()
export class BaseStore<T extends BaseModel> {
    private updating$ = new BehaviorSubject<boolean>(false);
    private storeData$ = new BehaviorSubject<T[] | null>(null);

    constructor() {
    }

    isUpdating$() {
        return this.updating$.asObservable();
    }

    setUpdating(isUpdating: boolean) {
        this.updating$.next(isUpdating);
    }

    getData$(): Observable<T[] | null> {
        return this.storeData$;
    }

    getDataById$(id: number): Observable<T | null> {
        // @ts-ignore
      // @ts-ignore
      const data = this.storeData$.asObservable().pipe(
            map((x: T[] | null) => {
                return x !== undefined && x !== null ? x.filter(value => value.id == id)[0] : null;
            })
        ) ;
        return data;
    }

    setData(data: T[]) {
        this.storeData$.next(data);
    }

    addData(data: T) {
        const currentValue = this.storeData$.getValue();
        if (!currentValue) {
            return;
        }
        this.storeData$.next([...currentValue, data]);
    }

    addRangeData(data: T[]) {
        const currentValue = this.storeData$.getValue();
        if (!currentValue) {
            return;
        }
        currentValue.push(...data);
        this.storeData$.next([...currentValue]);
    }

    updateData(updatedData: T) {
        const data = this.storeData$.getValue();
        if (!data) {
            return;
        }
        const indexOfUpdated = data.findIndex(rec => rec.id === updatedData.id);
        data[indexOfUpdated] = updatedData;
        this.storeData$.next([...data]);
    }

    removeData(dataRemove: T) {
        const currentValue = this.storeData$.getValue();
        if (!currentValue) {
            return;
        }
        this.storeData$.next(currentValue.filter(rec => rec !== dataRemove));
    }
}
