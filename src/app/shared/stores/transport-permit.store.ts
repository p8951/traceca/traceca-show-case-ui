import {BaseStore} from '@shared/stores/base.store';
import {ITransportPermit} from '@shared/models/transport-permit';
import {Injectable} from '@angular/core';

@Injectable()
export class TransportPermitStore extends BaseStore<ITransportPermit>{}
