import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from '@coreDirectory/auth/auth.guard';

const routes: Routes = [
  {path: '', redirectTo: '/public', pathMatch: 'full'},
  {
    path: 'login',
    loadChildren: () => import('src/app/features/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'register',
    loadChildren: () => import('src/app/features/register/register.module').then(m => m.RegisterModule)
  },
  {
    path: 'public',
    loadChildren: () => import('src/app/features/public/public.module').then(m => m.PublicModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('src/app/features/admin/admin.module').then(m => m.AdminModule),
    // canActivate: [AuthGuard]
  },
  {
    path: 'country',
    loadChildren: () => import('src/app/features/traceca/traceca.module').then(m => m.TracecaModule),
    // canActivate: [AuthGuard]
  },
  {
    path: 'carrier',
    loadChildren: () => import('src/app/features/carrier/carrier.module').then(m => m.CarrierModule),
    // canActivate: [AuthGuard]
  },
  {
    path: 'transport',
    loadChildren: () => import('src/app/features/transport/transport.module').then(m => m.TransportModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'inspector',
    loadChildren: () => import('src/app/features/inspector/inspector.module').then(m => m.InspectorModule),
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
