import {Component, OnInit} from '@angular/core';
import {AuthUser} from '@coreDirectory/auth/auth-user';
import {AuthenticationService} from '@coreDirectory/auth/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'traceca-show-case-ui';

  constructor(private authApi: AuthenticationService) {
  }
  ngOnInit(): void {
    this.authApi.setUserValue();
  }
}
