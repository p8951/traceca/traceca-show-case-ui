FROM node:16.14.2-alpine3.14 as build-env

WORKDIR /app
COPY package*.json ./

RUN npm config rm proxy
RUN npm config rm https-proxy
RUN npm install
COPY . .
RUN node --max-old-space-size=10240 ./node_modules/@angular/cli/bin/ng build --prod

FROM nginx:1.17.1-alpine

RUN echo -e " \n\
server { \n\
    listen 81;\n\
    root /usr/share/nginx/html;\n\
    client_max_body_size 10M;\n\
    location / {\n\
        try_files \$uri \$uri/ /index.html;\n\
    }\n\
}\n" > /etc/nginx/conf.d/default.conf
COPY --from=build-env /app/dist/traceca-show-case-ui /usr/share/nginx/html
